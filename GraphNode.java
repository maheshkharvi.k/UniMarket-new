package com.unimarket;

import java.util.Set;

public interface GraphNode {

	Set<Node> getDirectlyLinkedNodes(final Node node);

	int getShortestPathDistance(Node startNode, Node finishNode);

}
