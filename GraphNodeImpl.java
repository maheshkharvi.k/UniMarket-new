package com.unimarket;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class GraphNodeImpl implements GraphNode {

	@Override
	public int getShortestPathDistance(Node sourceNode, Node destinationNode) {
		// Initialization.
		Map<Node, Node> nextNodeMap = new HashMap<>();
		Node currentNode = sourceNode;
		// Queue
		Queue<Node> queue = new LinkedList<>();
		queue.add(currentNode);

		// Search.
		while (!queue.isEmpty()) {
			currentNode = queue.remove();
			if (currentNode.equals(destinationNode)) {
				break;
			} else {
				for (Node nextNode : getDirectlyLinkedNodes(currentNode)) {

					if (!queue.contains(nextNode)) {
						queue.add(nextNode);
						// Look up of next node instead of previous.
						nextNodeMap.put(currentNode, nextNode);

					}
				}
			}
		}

		// If all nodes are explored and the destination node hasn't been found.
		if (!currentNode.equals(destinationNode)) {
			throw new RuntimeException("No feasible path.");
		}
		int counter = 0;

		// Reconstruct path. No need to reverse.
		for (Node node = sourceNode; node != null; node = nextNodeMap.get(node)) {
			counter++;
		}
		return counter;
	}

	@Override
	public Set<Node> getDirectlyLinkedNodes(final Node node) {

		return null;
	}

}
